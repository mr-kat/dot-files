# ~/.bashrc
#

# If not running interactively, don't do anything
#[[ $- != *i* ]] && return

alias ls='ls --color=auto'
alias x='startx'
alias off='poweroff'
alias dec='xbacklight -dec 10'
alias inc='xbacklight -inc 10'
alias sy='sudo pacman -Syu' 
