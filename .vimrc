
set number




"vim-plug

call plug#begin('~/.vim/plugged')


"On-demand loading
Plug 'scrooloose/nerdtree'

" Shorthand notation; fetches https://github.com/junegunn/vim-easy-align
Plug 'junegunn/vim-easy-align'

" Any valid git URL is allowed
Plug 'junegunn/vim-github-dashboard'

"theme

Plug 'vim-airline/vim-airline'

Plug 'vim-airline/vim-airline-themes'

let g:airline_theme='simple'
let g:airline#extensions#tabline#enabled = 1

"emmet-vim

Plug 'mattn/emmet-vim'

call plug#end()
